const app = require("./routes/index.js")
const { fetchUserData } = require("./services/userData")
const config = require("./config.json")

//Jozzy
global["users"] = {}
global["users"]["5GCC7T"] = {}
global["users"]["5GCC7T"]["access_token"] = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIyMkRRN1YiLCJzdWIiOiI1R0NDN1QiLCJpc3MiOiJGaXRiaXQiLCJ0eXAiOiJhY2Nlc3NfdG9rZW4iLCJzY29wZXMiOiJ3aHIgd3BybyB3bnV0IHdzbGUgd3dlaSB3c29jIHdzZXQgd2FjdCB3bG9jIiwiZXhwIjoxNTYwNDU2MTA2LCJpYXQiOjE1NTk5ODEwMzN9.IF-455hGfbNyzGwcmZ2Dl_6CswmwfyberA1HcWXc3tM"

global['directory'] = __dirname

app.listen(config.port, () => console.log(`App running on port ${config.port}!`))