const http = require("http")

const post = (options, data, callback) => {
    options["method"] = "post"
    var request = http.request(options, function(res) {
        res.setEncoding("utf8")
        var response = ""
        res.on("data", (data) => {
            response += data
        })
        res.on("end", () => {
            data = JSON.parse(response)
            if (data["success"] === false) {
                callback(data.errors, null)
            } else {
                callback(null, data)
            }
        })
    }, function(err) {
        console.log(`caught error ${activity}`, err)
        callback(data.errors, null)
    })
    request.write(data)
    request.end()
}

module.exports = {
    post
}