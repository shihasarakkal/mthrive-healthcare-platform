const express = require('express')
const app = module.exports = express();
const bodyParser = require('body-parser')
const cors = require('cors')
const getFitBitUserAccessToken = require('../services/FitBit/authorize').getAccessToken
const { fetchUserData } = require("../services/userData")
const path = require('path');

app.use(cors())
app.use(bodyParser.json())
app.use(express.static('static'))

app.get('/get-fitbit-access-token', ({query: {code}}, res) => {
    getFitBitUserAccessToken(code)
    res.status(200).send("authorizing")
})

app.get('/quotes', (req, res) => {
    res.sendFile(path.join(global.directory + '/static/index.html'));
})

app.get('/get-quotes', (req, res) => {
    res.status(200).json([
        {
            name: "Alice",
            monthly: 5265,
            healthTrend: "positive",
            healthScore: 2,
            incentive: 1570,
            type: "Health",
            payable: 5265 - 1570
        },
        {
            name: "Bob",
            monthly: 6785,
            healthTrend: "positive",
            healthScore: 0,
            incentive: 1150,
            type: "Health",
            payable: 5265 - 1570
        }
    ])
})

app.get('/fitbit-subscriptions', function(req, res) {
  if (req.query.verify === "ca4827487271f5c00b5f82e8aec535543722ecbb05241670ab95a3b9efbfc1cf") {
    res.status(204).send()
  } else {
    res.status(404).send()
  }
});

app.post('/fitbit-subscriptions', function({body: notifications}, res) {
    res.status(204).send()
    notifications.forEach(notification => {
        fetchUserData(notification["ownerId"])
    })
});
