const elasticsearch = require('elasticsearch')
const config = require('../../config')
const moment = require('moment');

var client = new elasticsearch.Client({
  host: config.elastic.client.host,
  log: "trace"
});

index = (index, {data: body}) => {
    client.index({
        index,
        body
    })
}

bulk = (data) => {
    client.bulk({
        body: data
    }, (err, res) => {
//        console.log({err})
    })
}


_bulk = (_index, data) => {
    console.log({data: data.length})
    var body = []
    data.forEach((d, index) => {
        var time = new Date()
        if (d["body"]["effective_time_frame"]) {
            time = d["body"]["effective_time_frame"]["date_time"] || d["body"]["effective_time_frame"]["time_interval"]["start_date_time"]
        }

        body.push({
            index: {
                _index,
                _type: "fitbit",
                _id: `${d['header']['user_id']}_${moment(time).format('YYYY-MM-DD')}`
            }
        })
        body.push(d)
    })
    client.bulk({
        body
    }, (err, res) => {
//        console.log({err})
    })
}


module.exports = {
    bulk,
    _bulk
}