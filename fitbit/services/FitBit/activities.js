const config = require("../../config")
const https = require("https")
const fs = require('fs');

getActivityTimeSeries = (userId, activity, baseDate, endDate, callback) => {
    const accessToken = global["users"][userId]["access_token"]
    const headers = {
        "Authorization": `Bearer ${accessToken}`,
        "Content-Type": "application/x-www-form-urlencoded"
    }
    const reqOptions = {
        host: config.fitbit.baseSite,
        port: "443",
        path:  config.fitbit.API.activitiesDateRange.replace("[user-id]", userId).replace('[resource-path]', `activities/${activity}`).replace('[base-date]', baseDate).replace('[end-date]', endDate),
        method: "get",
        headers
    }
    var getActivitiesTimeSeriesReq = https.request(reqOptions, function(res) {
        res.setEncoding("utf8")
        var response = ""
        res.on("data", (data) => {
            response += data
        })
        res.on("end", () => {
            data = JSON.parse(response)
            if (data["success"] === false) {
                callback(data.errors, null)
            } else {
                callback && callback(null, data)
            }
        })
    }, function(err) {
        console.log(`caught error ${activity}`, err)
    })
    getActivitiesTimeSeriesReq.end()
}

getActivitySummary = (userId) => {
    console.log('trying to get activity summary')
    const accessToken = global["users"][userId]["access_token"]
    const headers = {
        "Authorization": `Bearer ${accessToken}`,
        "Content-Type": "application/x-www-form-urlencoded"
    }
    let date = new Date()
    console.log({url: config.fitbit.API.activitySummary.replace("[user-id]", userId).replace('[date]', `${date.getFullYear()}-0${date.getMonth()+1}-${date.getDate()}`)})
    const reqOptions = {
        host: config.fitbit.baseSite,
        port: "443",
        path:  config.fitbit.API.activitySummary.replace("[user-id]", userId).replace('[date]', `${date.getFullYear()}-0${date.getMonth()+1}-0${date.getDay()}`),
        method: "get",
        headers
    }
    var getActivitiesTimeSeriesReq = https.request(reqOptions, function(res) {
        res.setEncoding("utf8")
        var response = ""
        res.on("data", (data) => {
            response += data
        })
        res.on("end", () => {
            data = JSON.parse(response)
            if (data["success"] === false) {
                console.error(`Failed to fetch activity Summary`, data.errors)
            } else {
                console.log(`Activity summary: ${JSON.stringify(data, null, 2)}`)
            }
        })
    }, function(err) {
        console.log(`caught error ${activity}`, err)
    })
    getActivitiesTimeSeriesReq.end()
}

module.exports = {
    getActivityTimeSeries,
    getActivitySummary
}