const https = require('https')
const queryString = require('querystring')
const config = require('../../config')

getAccessToken = (code, callback) => {

    const buff = new Buffer(`${config.fitbit.OAuth.clientId}:${config.fitbit.OAuth.clientSecret}`)
    const baseToken = buff.toString('base64')
    const getAccessTokenReqHeaders = {
        "Authorization": `Basic ${baseToken}`,
        "Content-Type": "application/x-www-form-urlencoded"
    }
    const getAccessTokenReqData = queryString.stringify({
        "client_id": config.fitbit.OAuth.clientSecret,
        "grant_type": "authorization_code",
        "redirect_uri": config.fitbit.OAuth.redirectURI,
        code
    })
    const getAccessTokenReqOptions = {
        host: config.fitbit.baseSite,
        port: "443",
        path:  config.fitbit.OAuth.accessTokenPath,
        method: "post",
        headers: getAccessTokenReqHeaders
    }
    
    var getAccessTokenReq = https.request(getAccessTokenReqOptions, function(res) {
        res.setEncoding("utf8")
        res.on("data", (data) => {
            data = JSON.parse(data)
            if (data["success"] === false) {
                console.error('Failed to fetch access token', data.errors)
            } else {
                console.log("Response: ", data)
                global["users"] = global["users"] || {}
                global["users"][data["user_id"]] = data
                callback && callback()
            }
        })
    }, function(err) {
        console.log('caught error', err)
    })
    
    getAccessTokenReq.write(getAccessTokenReqData)
    getAccessTokenReq.end()

}

module.exports = {
    getAccessToken
}