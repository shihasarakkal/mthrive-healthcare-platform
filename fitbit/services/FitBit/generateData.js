//const { getRandomInt } = require('../../utils/utils')
const moment = require('moment');

dataSeries = (activity, unit, startDate, endDate, values, aggregation, user_id) => {
    var date = startDate = new Date(startDate)
    endDate = new Date(endDate)
    var index = 0
    var outStr = ""
    var heartRate = []
    do {
        heartRate.push({
            header: {
                user_id
            },
            body: {
                [activity]: {
                    value: parseInt(values[index++]),
                    unit
                },
                effective_time_frame: {
                    "date_time": moment(date).startOf('day').utc().format()
                },
                temporal_relationship_to_physical_activity: "at_rest"
            }
        })
        date = new Date(date.setDate(date.getDate() + 1))
    } while(date.getTime() < endDate.getTime())
    return heartRate
}

activitySeries = (activity, unit, startDate, endDate, values, aggregation, user_id) => {
    var date = startDate = new Date(startDate)
    endDate = new Date(endDate)
    var index = 0
    var dataSeries = []
    var activityData
    do {
     if (unit) {
        activityData = {
            value: values[index++] || values[0],
            unit: unit
        }
     } else {
        activityData = values[index++] || values[0]
     }
     dataSeries.push({
         header: {
             user_id
         },
         body: {
             [activity]: activityData,
             effective_time_frame: {
                time_interval: {
                    "start_date_time": moment(date).startOf('day').utc().format(),
                    "end_date_time": moment(date).endOf('day').utc().format()
                }
             },
             temporal_relationship_to_physical_activity: "at_rest"
         }
     })
     date = new Date(date.setDate(date.getDate() + 1))
    } while(date.getTime() < endDate.getTime())
    return dataSeries
 }

sleepSeries = (activity, unit, startDate, endDate, data, aggregation, user_id) => {
    var date = startDate = new Date(startDate)
    endDate = new Date(endDate)
    var index = 0
    var dataSeries = []
    var activityData
    do {
        activityData = {
            "deep": {
                "minutes": data["deep"][index]
            },
            "light": {
                "minutes": data["light"][index]
            },
            "rem": {
                "minutes": data["rem"][index]
            },
            "wake": {
                "minutes": data["wake"][index++]
            }
        }
         dataSeries.push({
             header: {
                 user_id
             },
             body: {
                 [activity]: activityData,
                 effective_time_frame: {
                    time_interval: {
                        "start_date_time": moment(date).startOf('day').utc().format(),
                        "end_date_time": moment(date).endOf('day').utc().format()
                    }
                 }
             }
         })
        date = new Date(date.setDate(date.getDate() + 1))
    } while(date.getTime() < endDate.getTime())
    return dataSeries
}

bloodPressureSeries = (startDate, endDate, data, aggregation, user_id) => {
    var date = startDate = new Date(startDate)
    endDate = new Date(endDate)
    var index = 0
    var dataSeries = []
    var activityData
    do {
        activityData = {

        }
         dataSeries.push({
             header: {
                 user_id
             },
             body: {
                 "systolic_blood_pressure": {
                    value: data["systolic"][index],
                    unit: "mmHg"
                 },
                 "diastolic_blood_pressure": {
                    value: data["diastolic"][index++],
                    unit: "mmHg"
                 },
                 effective_time_frame: {
                    time_interval: {
                        "start_date_time": moment(date).startOf('day').utc().format(),
                        "end_date_time": moment(date).endOf('day').utc().format()
                    }
                 }
             }
         })
        date = new Date(date.setDate(date.getDate() + 1))
    } while(date.getTime() < endDate.getTime())
    return dataSeries
}

module.exports = {
    dataSeries,
    activitySeries,
    sleepSeries,
    bloodPressureSeries
}