const config = require("../../config")
const https = require("https")

getSleepTimeRangeData = (userId, baseDate, endDate, callback) => {
    const accessToken = global["users"][userId]["access_token"]
    const headers = {
        "Authorization": `Bearer ${accessToken}`,
        "Content-Type": "application/x-www-form-urlencoded"
    }
    const reqOptions = {
        host: config.fitbit.baseSite,
        port: "443",
        path:  config.fitbit.API.sleepDateRangeData.replace("[user-id]", userId).replace('[baseDate]', baseDate).replace('[endDate]', endDate),
        method: "get",
        headers
    }
    console.log({reqOptions})
    var getHearRateReq = https.request(reqOptions, function(res) {
        res.setEncoding("utf8")
        var response = ""
        res.on("data", (data) => {
            response += data
        })
        res.on("end", () => {
            data = JSON.parse(response)
            if (data["success"] === false) {
                callback(data.errors, null)
            } else {
                callback(null, data)
            }
        })
    }, function(err) {
        callback(err, null)
    })
    getHearRateReq.end()
}

module.exports = {
    getSleepTimeRangeData
}