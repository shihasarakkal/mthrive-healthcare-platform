const config = require('../../config')
const https = require("https")

getUserInfo = (userId) => {
    const accessToken = global["users"][userId]["access_token"]
    const headers = {
        "Authorization": `Bearer ${accessToken}`,
        "Content-Type": "application/x-www-form-urlencoded"
    }
    const reqOptions = {
        host: config.fitbit.baseSite,
        port: "443",
        path:  config.fitbit.API.userProfile,
        method: "get",
        headers
    }
    var getUserInfoReq = https.request(reqOptions, function(res) {
        res.setEncoding("utf8")
        resData = ""
        res.on("data", (data) => {
            resData += data
        })
        res.on("end", () => {
            data = JSON.parse(resData)
            if (data["success"] === false) {
                console.error("Failed to fetch user info", data.errors)
            } else {
                console.log("Response: ", data)
            }
        })
    }, function(err) {
        console.log("caught error", err)
    })
    getUserInfoReq.end()
}

module.exports = {
    getUserInfo
}