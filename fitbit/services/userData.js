const { getActivityTimeSeries } = require("./FitBit/activities")
const { getSleepTimeRangeData } = require("./FitBit/sleep")
const { convertActivityDataToElasticSchema } = require("../utils/dataUtils")
const { getRestingHeartRate } = require('./FitBit/heartRate')
const { bulk: elasticBulkPush } = require('./ElasticSearch/client.js')

const moment = require('moment');

const config = require('../config')

fetchUserData = (userId) => {
    fetchUserActivities(userId)
    fetchUserHeartRate(userId)
}

fetchUserHeartRate = (userId) => {
    var now = new Date()
    var baseDate = moment(now.setDate(now.getDate() - 60)).format('YYYY-MM-DD')
    var endDate = moment(now).format('YYYY-MM-DD')
    getRestingHeartRate(userId, baseDate, endDate, (err, data) => {
        if (err) {
            console.log('error fetching heartrate', err)
        } else {
            var heartData = []
            data["activities-heart"].forEach((data) => {
              if (data["value"]["restingHeartRate"]) {
                heartData.push({
                    header: {
                        user_id: userId
                    },
                    body: {
                        'heart_rate': {
                            value: data["value"]["restingHeartRate"],
                            unit: "beats/min"
                        },
                        "effective_time_frame": {
                            "date_time": data["dateTime"]
                        }
                    }
                })
              }
            })
        }
    })
}

fetchUserSleepData = (userId) => {
    var now = new Date()
    var startDate = moment(now.setMonth(now.getMonth() - 2)).format('YYYY-MM-DD')
    var endDate = moment().format('YYYY-MM-DD')
    getSleepTimeRangeData(userId, startDate, endDate, (err, data) => {
        if (err) {
            console.log('error fetching data', err)
        } else {
            data["sleep"].forEach(sleep => {
                console.log({sleep: JSON.stringify(sleep)})
            })
        }
    })
}

fetchUserActivities = (userId) => {
    var requiredActivities = [
        {
            activity: "calories",
            openMLName: "kcal_burned",
            unit: "kcal"
        },
        {
            activity: "steps",
            openMLName: "step_count"
        }
    ]
    var now = new Date()
    var startDate = moment(now.setMonth(now.getMonth() - 2)).format('YYYY-MM-DD')
    var endDate = moment().format('YYYY-MM-DD')
    requiredActivities.forEach(activityDetails => {
        getActivityTimeSeries(userId, activityDetails["activity"], '2019-04-01', moment().format('YYYY-MM-DD'), (err, data) => {
        if (err) {
            console.log('Error fetching fitbit calories', err)
        } else {
            var elasticIndex = config["elastic"]["indexes"][activityDetails["activity"]]
            var activityData = convertActivityDataToElasticSchema(data, activityDetails, userId, elasticIndex)
            console.log(activityData[0]["index"])
            console.log(activityData[1]["body"])
            elasticBulkPush(activityData)
        }
        })
    })
}

module.exports = {
    fetchUserData
}