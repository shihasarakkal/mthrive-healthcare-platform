console.log('hai', {ajax: $.ajax})

var wrapper = $("#wrapper")

console.log({wrapper})

var quotes = $.ajax({
    url: "/get-quotes",
    cache: false,
    success: (data) => {
        data.forEach(d => {
            var quoteElement = $(document.createElement('DIV'))
                .addClass("quote-element")

            var logoWrapper = $(document.createElement('DIV'))
                .addClass('logo-wrapper block')
            var logo = $(document.createElement('img'))
                .addClass('logo')
                .attr("src","logo.png")
                .appendTo(logoWrapper);
//            var box1Text1 =  $(document.createElement('h3'))
//                .addClass('text')
//                .text("Company name")
//                .appendTo(logoWrapper);
            var checkBoxWrapper =  $(document.createElement('span'))
                .addClass('checkbox-wrapper')
                .appendTo(logoWrapper);
            var checkbox =  $(document.createElement('input'))
                .attr("type", "checkbox")
                .addClass('checkbox')
                .appendTo(checkBoxWrapper);
            var box1Text2 =  $(document.createElement('span'))
                .addClass('text')
                .text('compare')
                .appendTo(checkBoxWrapper);

            quoteElement.append(logoWrapper)


            var deductible = $(document.createElement('DIV'))
                .addClass("deduct block")
            var box2Text1 =  $(document.createElement('h4'))
                .addClass('text')
                .text("Insurance type")
                .appendTo(deductible);
            var deductibleTxt = $(document.createElement('span'))
                .addClass('text')
                .text(d["type"])
                .appendTo(deductible)
            quoteElement.append(deductible)

             var monthlyCost = $(document.createElement('DIV'))
                .addClass("monthly-cost block")
            var box3Text1 =  $(document.createElement('h4'))
                .addClass('text')
                .text("Expected monthly cost")
                .appendTo(monthlyCost);
             var monthlyCostTxt = $(document.createElement('span'))
                .addClass('text')
                .text(d["monthly"])
                .appendTo(monthlyCost)
             quoteElement.append(monthlyCost)

            var incentive = $(document.createElement('DIV'))
                .addClass("incentive block")
            var box4Text1 =  $(document.createElement('h4'))
                .addClass('text')
                .text("Incentive based on health improvement")
                .appendTo(incentive);
            var incentiveTxt = $(document.createElement('span'))
                .addClass('text')
                .text(d["monthly"])
                .appendTo(incentive)
            quoteElement.append(incentive)

            var payable = $(document.createElement('DIV'))
                .addClass("payable block")
            var box5Text1 =  $(document.createElement('h4'))
                .addClass('text')
                .text("Actual payable")
                .appendTo(payable);
            var payableTxt = $(document.createElement('span'))
                .addClass('text')
                .text(d["payable"])
                .appendTo(payable)
            quoteElement.append(payable)

            quoteElement.appendTo(wrapper)
        })
    }
});