const moment = require('moment');

convertActivityDataToElasticSchema = (data, activityDetails, userId, elasticIndex) => {
    var convertedData = []
    data[`activities-${activityDetails["activity"]}`].forEach((d, _index) => {
        var activityData
        if (activityDetails["unit"]) {
            activityData = {
                value: parseFloat(d["value"]),
                unit: activityDetails["unit"]
            }
        } else {
            activityData = parseFloat(d["value"])
        }
        convertedData.push({
           index: {
               _index: elasticIndex,
               _type: "fitbit",
               _id: `${userId}_${moment(d["dateTime"]).format('YYYY-MM-DD')}`
           }
        })
        convertedData.push({
           header: {
               user_id: userId
           },
           body: {
               [activityDetails['openMLName']]: activityData,
               effective_time_frame: {
                   time_interval: {
                        "start_date_time": moment(d["dateTime"]).startOf('day').utc().format(),
                        "end_date_time": moment(d["dateTime"]).endOf('day').utc().format()
                   }
               }
           }
        })
    })
    return convertedData
}

module.exports = {
    convertActivityDataToElasticSchema
}