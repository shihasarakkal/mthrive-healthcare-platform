package com.googlefit.googlefit;

import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.googlefit.googlefit.model.Token;

@Component
public class GoogleFitAuth {

	public static String accessToken;
	
	public String getAccessToken(){
		
		String url = "https://developers.google.com/oauthplayground/refreshAccessToken";
		RestTemplate restTemplate = new RestTemplate();
	    	    
		Token token = new Token();
		token.setToken_uri("https://www.googleapis.com/oauth2/v4/token");
		token.setRefresh_token("1/rGvizeW92GrhXN2_I4BHXK_icK1-3eOcHi5pmKt7q7Q");
		Map<String,Object> result = (Map<String, Object>) restTemplate.postForObject(url, token, Object.class);
		accessToken = (String) result.get("access_token");
		return accessToken;
	}
}
