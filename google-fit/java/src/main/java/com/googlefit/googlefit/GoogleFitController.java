package com.googlefit.googlefit;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.googlefit.googlefit.model.EffectiveTimeFrame;
import com.googlefit.googlefit.model.MHealthBody;
import com.googlefit.googlefit.model.MHealthHeader;
import com.googlefit.googlefit.model.MHealthStep;
import com.googlefit.googlefit.model.ReqPayload;
import com.googlefit.googlefit.model.Request;
import com.googlefit.googlefit.model.StepCountAggregate;
import com.googlefit.googlefit.model.TimeInterval;
import com.googlefit.googlefit.model.elastic.BodyFat;
import com.googlefit.googlefit.model.elastic.BodyFatPercentage;
import com.googlefit.googlefit.model.elastic.EIndex;

@RestController
public class GoogleFitController {

	@Autowired
	GoogleFitAuth googleFitAuth;
	
	@Autowired
	Utility utility;
	

	
	@RequestMapping("/steps")
	public ResponseEntity<Object> getData() throws UnsupportedEncodingException{
		String url = "https://developers.google.com/oauthplayground/sendRequest";
		RestTemplate restTemplate = new RestTemplate();
		if(GoogleFitAuth.accessToken == null){
			googleFitAuth.getAccessToken();
		}
		Request request = new Request("https://www.googleapis.com/fitness/v1/users/me/dataSources/derived:com.google.step_count.delta:com.google.android.gms:estimated_steps/datasets/1559333400000000000-1559334480000000000", googleFitAuth.accessToken);
		MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
		jsonHttpMessageConverter.getObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		restTemplate.getMessageConverters().add(jsonHttpMessageConverter);
		String token = googleFitAuth.getAccessToken();
		
		/*String data = "{\"Method\":\"GET\",\"absoluteURI\":\"https://www.googleapis.com/fitness/v1/users/me/dataSources/derived:com.google.step_count.delta:com.google.android.gms:estimated_steps/datasets/1559333400000000000-1559334480000000000\",\"headers\":{},\"message-body\":\"\",\"access_token\":\"ya29.GlshBwDWbTyAleDkibe8xDptIf1KhMOeh3iH9pGAX_KZ0UoER838ap8kROXmWcaaXCPrAPcsPr93aQUHTsJ0E3bLi_044dVhSiJFCsiqWMAR5M1lRa8ZQpisftW_\",\"access_token_type\":\"bearer\"}";
		Object d = data;*/
		Map<String,Object> result = (Map<String, Object>) restTemplate.postForObject(url, genearteRequestBody(), Object.class);
		Map<String,Object> response = (Map<String, Object>) result.get("Response");
		if((int)response.get("Status-Code") == 401){
			googleFitAuth.getAccessToken();
		}
		String a = new String(Base64Utils.decodeFromString((String)response.get("message-body")), "UTF-8");
		JSONObject jsonObj = new JSONObject(a);
		JSONArray bucket =  (JSONArray) jsonObj.get("bucket");
		JSONObject val = (JSONObject) bucket.get(0);
		StepCountAggregate stepCountAggregate = convertToStepCountAggregate(val);
		MHealthStep mHealthStep = new MHealthStep(new MHealthHeader("lemon"), new MHealthBody(stepCountAggregate)); 
		
		sendtoElasticSearch(mHealthStep);
		return new ResponseEntity<Object>(Base64Utils.decodeFromString((String)response.get("message-body")),HttpStatus.OK);
	}
	
	private void sendtoElasticSearch(MHealthStep mHealthStep) {
		/*IndexResponse response = client.prepareIndex("steps", "steps")
                	.setSource(mHealthStep.toString(),XContentType.JSON)
                .get();*/
	}

	private StepCountAggregate convertToStepCountAggregate(JSONObject val) {
		String start_date_time = val.getString("startTimeMillis");
		String end_date_time = val.getString("endTimeMillis");
		int step_count = getStepCount(val.getJSONArray("dataset"));
		StepCountAggregate stepCountAggregate = new StepCountAggregate(step_count, new EffectiveTimeFrame(new TimeInterval(start_date_time, end_date_time)));
		return stepCountAggregate;
	}

	private int getStepCount(JSONArray jsonArray) {
		JSONObject point = (JSONObject) jsonArray.get(0);
		JSONArray point1 = (JSONArray) point.get("point");
		JSONObject value = (JSONObject) point1.get(0);
		JSONArray valueArray = (JSONArray) value.get("value");
		JSONObject stepValue = (JSONObject) valueArray.get(0);
		return (int)stepValue.get("intVal");
	}

	private Object genearteRequestBody(){
		if(GoogleFitAuth.accessToken == null){
			googleFitAuth.getAccessToken();
		}
		String token = GoogleFitAuth.accessToken;
		//String reqPayload ="{\"Method\":\"POST\",\"absoluteURI\":\"https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate\",\"headers\":{\"Content-Type\":\"application/json\",\"Content-Length\":\"296\"},\"message-body\":\"{\\n  \\\"aggregateBy\\\": [{\\n    \\\"dataTypeName\\\": \\\"com.google.step_count.delta\\\",\\n    \\\"dataSourceId\\\": \\\"derived:com.google.step_count.delta:com.google.android.gms:estimated_steps\\\"\\n  }],\\n  \\\"bucketByTime\\\": { \\\"durationMillis\\\": 86400000 },\\n  \\\"startTimeMillis\\\": "+utility.getStartDateinMs()+",\\n  \\\"endTimeMillis\\\": "+utility.getEndDateinMs()+"\\n}\",\"access_token\":\""+token+"\",\"access_token_type\":\"bearer\"}";
		String reqPayload ="{\"Method\":\"POST\",\"absoluteURI\":\"https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate\",\"headers\":{\"Content-Type\":\"application/json\",\"Content-Length\":\"296\"},\"message-body\":\"{\\n  \\\"aggregateBy\\\": [{\\n    \\\"dataTypeName\\\": \\\"com.google.step_count.delta\\\",\\n    \\\"dataSourceId\\\": \\\"derived:com.google.step_count.delta:com.google.android.gms:estimated_steps\\\"\\n  }],\\n  \\\"bucketByTime\\\": { \\\"durationMillis\\\": 86400000 },\\n  \\\"startTimeMillis\\\": 1559586600000,\\n  \\\"endTimeMillis\\\": 1559673000000\\n}\",\"access_token\":\""+token+"\",\"access_token_type\":\"bearer\"}";

		return reqPayload;
	}
	
	
	
	@RequestMapping(method = RequestMethod.POST , value="/data")
	public ResponseEntity<Object> pushDataToGoogleFit(
			@RequestBody ReqPayload reqPayload) throws IOException{
		if(reqPayload.getSource().equals("googlefit")){
			String url = "https://developers.google.com/oauthplayground/sendRequest";
			RestTemplate restTemplate = new RestTemplate();
			
			MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
			jsonHttpMessageConverter.getObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
			restTemplate.getMessageConverters().add(jsonHttpMessageConverter);
			
			createSensorReq(reqPayload);
			Map<String,Object> result = (Map<String, Object>) restTemplate.postForObject(url, createSensorReq(reqPayload), Object.class);
			Map<String,Object> response = (Map<String, Object>) result.get("Response");
		}
		
		
		if(reqPayload.getSource().equals("elasticsearch")){
			BodyFat bodyFat = new BodyFat(new EIndex("asd"),new BodyFatPercentage(reqPayload.getValue()), new com.googlefit.googlefit.model.elastic.EffectiveTimeFrame(new Date().toString()));
			//mHealthDao.insertData(bodyFat);
			RestTemplate restTemplate = new RestTemplate();
			MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
			jsonHttpMessageConverter.getObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
			restTemplate.getMessageConverters().add(jsonHttpMessageConverter);
			
			List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
			interceptors.add(new HeaderRequestInterceptor("Content-Type", MediaType.APPLICATION_JSON_VALUE));
			restTemplate.setInterceptors(interceptors);
			
			ObjectMapper Obj = new ObjectMapper(); 
			 String jsonStr = Obj.writeValueAsString(bodyFat);
			
			restTemplate.postForEntity("http://192.168.63.170:9200", jsonStr, String.class);
		}
		
		
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	private Object createSensorReq(ReqPayload reqPayload) {
		String req=null;
		if(reqPayload.getType().equals("body_fat_percentage")){
			req = "{\"Method\":\"PATCH\",\"absoluteURI\":\"https://www.googleapis.com/fitness/v1/users/me/dataSources/derived:body_fat_percentage:407408718192:custom:custom:1234/datasets/"+reqPayload.getStartTime()+"-"+reqPayload.getEndTime()+"\",\"headers\":{\"Content-Type\":\"application/json\",\"Content-Length\":\"441\"},\"message-body\":\"{  \\\"minStartTimeNs\\\": \\\""+reqPayload.getStartTime()+"\\\",   \\\"maxEndTimeNs\\\": \\\""+reqPayload.getEndTime()+"\\\",   \\\"dataSourceId\\\": \\\"derived:body_fat_percentage:407408718192:custom:custom:1234\\\",   \\\"point\\\": [    {      \\\"startTimeNanos\\\": \\\""+reqPayload.getStartTime()+"\\\",       \\\"endTimeNanos\\\": \\\""+reqPayload.getEndTime()+"\\\",       \\\"value\\\": [        {         \\\"fpVal\\\": \\\""+reqPayload.getValue()+"\\\"        }      ],       \\\"dataTypeName\\\": \\\"body_fat_percentage\\\",       \\\"originDataSourceId\\\": \\\"\\\"    }  ]}\",\"access_token\":\""+googleFitAuth.getAccessToken()+"\",\"access_token_type\":\"bearer\"}";
		}
		else if(reqPayload.getType().equals("body_height")){
			req = "{\"Method\":\"PATCH\",\"absoluteURI\":\"https://www.googleapis.com/fitness/v1/users/me/dataSources/derived:body_height:407408718192:custom:custom:1234/datasets/"+reqPayload.getStartTime()+"-"+reqPayload.getEndTime()+"\",\"headers\":{\"Content-Type\":\"application/json\",\"Content-Length\":\"426\"},\"message-body\":\"{  \\\"minStartTimeNs\\\": \\\""+reqPayload.getStartTime()+"\\\",   \\\"maxEndTimeNs\\\": \\\""+reqPayload.getEndTime()+"\\\",   \\\"dataSourceId\\\": \\\"derived:body_height:407408718192:custom:custom:1234\\\",   \\\"point\\\": [    {      \\\"startTimeNanos\\\": \\\""+reqPayload.getStartTime()+"\\\",       \\\"endTimeNanos\\\": \\\""+reqPayload.getEndTime()+"\\\",       \\\"value\\\": [        {         \\\"fpVal\\\": \\\""+reqPayload.getValue()+"\\\"        }      ],       \\\"dataTypeName\\\": \\\"body_height\\\",       \\\"originDataSourceId\\\": \\\"\\\"    }  ]}\",\"access_token\":\""+googleFitAuth.getAccessToken()+"\",\"access_token_type\":\"bearer\"}";
		}
		else if(reqPayload.getType().equals("body_mass_index")){
			req = "{\"Method\":\"PATCH\",\"absoluteURI\":\"https://www.googleapis.com/fitness/v1/users/me/dataSources/derived:body_mass_index:407408718192:custom:custom:1234/datasets/"+reqPayload.getStartTime()+"-"+reqPayload.getEndTime()+"\",\"headers\":{\"Content-Type\":\"application/json\",\"Content-Length\":\"433\"},\"message-body\":\"{  \\\"minStartTimeNs\\\": \\\""+reqPayload.getStartTime()+"\\\",   \\\"maxEndTimeNs\\\": \\\""+reqPayload.getEndTime()+"\\\",   \\\"dataSourceId\\\": \\\"derived:body_mass_index:407408718192:custom:custom:1234\\\",   \\\"point\\\": [    {      \\\"startTimeNanos\\\": \\\""+reqPayload.getStartTime()+"\\\",       \\\"endTimeNanos\\\": \\\""+reqPayload.getEndTime()+"\\\",       \\\"value\\\": [        {         \\\"fpVal\\\": \\\""+reqPayload.getValue()+"\\\"        }      ],       \\\"dataTypeName\\\": \\\"body_mass_index\\\",       \\\"originDataSourceId\\\": \\\"\\\"    }  ]}\",\"access_token\":\""+googleFitAuth.getAccessToken()+"\",\"access_token_type\":\"bearer\"}";
		}
		else if(reqPayload.getType().equals("body_weight")){
			req = "{\"Method\":\"PATCH\",\"absoluteURI\":\"https://www.googleapis.com/fitness/v1/users/me/dataSources/derived:body_weight:407408718192:custom:custom:1234/datasets/"+reqPayload.getStartTime()+"-"+reqPayload.getEndTime()+"\",\"headers\":{\"Content-Type\":\"application/json\",\"Content-Length\":\"425\"},\"message-body\":\"{  \\\"minStartTimeNs\\\": \\\""+reqPayload.getStartTime()+"\\\",   \\\"maxEndTimeNs\\\": \\\""+reqPayload.getEndTime()+"\\\",   \\\"dataSourceId\\\": \\\"derived:body_weight:407408718192:custom:custom:1234\\\",   \\\"point\\\": [    {      \\\"startTimeNanos\\\": \\\""+reqPayload.getStartTime()+"\\\",       \\\"endTimeNanos\\\": \\\""+reqPayload.getEndTime()+"\\\",       \\\"value\\\": [        {         \\\"fpVal\\\": \\\""+reqPayload.getValue()+"\\\"        }      ],       \\\"dataTypeName\\\": \\\"body_weight\\\",       \\\"originDataSourceId\\\": \\\"\\\"    }  ]}\",\"access_token\":\""+googleFitAuth.getAccessToken()+"\",\"access_token_type\":\"bearer\"}";
		}
		return req;
	}
	
	
}
