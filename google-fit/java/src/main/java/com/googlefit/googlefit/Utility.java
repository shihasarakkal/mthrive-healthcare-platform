package com.googlefit.googlefit;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class Utility {

	public String getStartDateinMs(){
		 Calendar now = Calendar.getInstance();
	        now.set(Calendar.HOUR, 0);
	        now.set(Calendar.MINUTE, 0);
	        now.set(Calendar.SECOND, 0);
	        now.set(Calendar.HOUR_OF_DAY, 0);
		return String.valueOf(now.getTimeInMillis());	
	}
	
	public String getEndDateinMs(){
		 Calendar now = Calendar.getInstance();
	        now.set(Calendar.HOUR, 0);
	        now.set(Calendar.MINUTE, 0);
	        now.set(Calendar.SECOND, 0);
	        now.set(Calendar.HOUR_OF_DAY, 0);
		return String.valueOf(now.getTimeInMillis()+86400000);	
	}
}
