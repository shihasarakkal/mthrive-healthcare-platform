package com.googlefit.googlefit.model;

public class EffectiveTimeFrame {

	private TimeInterval time_interval;

	public EffectiveTimeFrame(TimeInterval time_interval){
		this.time_interval = time_interval;
	}
	public TimeInterval getTime_interval() {
		return time_interval;
	}

	public void setTime_interval(TimeInterval time_interval) {
		this.time_interval = time_interval;
	}
}
