package com.googlefit.googlefit.model;

public class MHealthBody {

	
	private StepCountAggregate stepCountAggregate;
	
	public MHealthBody(StepCountAggregate stepCountAggregate){
		this.stepCountAggregate = stepCountAggregate;
	}

	public StepCountAggregate getStepCountAggregate() {
		return stepCountAggregate;
	}

	public void setStepCountAggregate(StepCountAggregate stepCountAggregate) {
		this.stepCountAggregate = stepCountAggregate;
	}
}
