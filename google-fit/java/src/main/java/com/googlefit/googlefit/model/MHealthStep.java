package com.googlefit.googlefit.model;


public class MHealthStep {

	
	private MHealthHeader header;
	
	private MHealthBody body;
	
	public MHealthHeader getHeader() {
		return header;
	}

	public void setHeader(MHealthHeader header) {
		this.header = header;
	}

	public MHealthBody getBody() {
		return body;
	}

	public void setBody(MHealthBody body) {
		this.body = body;
	}

	public MHealthStep(MHealthHeader header , MHealthBody body){
		this.header = header;
		this.body = body;
	}
}
