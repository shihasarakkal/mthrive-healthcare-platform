package com.googlefit.googlefit.model;

public class Request {

	private String Method;
	private String absoluteURI;
	private String access_token;
	private String access_token_type;
	
	//private Object headers;
	
	public Request(String absoluteURI , String access_token){
		Method = "GET";
		access_token_type = "bearer";
	
		this.absoluteURI=absoluteURI;
		this.access_token=access_token;
	}
}
