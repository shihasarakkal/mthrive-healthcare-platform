package com.googlefit.googlefit.model;

public class StepCountAggregate {

	private int step_count;
	
	private EffectiveTimeFrame effective_time_frame;

	
	public StepCountAggregate(int step_count , EffectiveTimeFrame effective_time_frame){
		this.step_count=step_count;
		this.effective_time_frame=effective_time_frame;
	}
	
	public int getStep_count() {
		return step_count;
	}

	public void setStep_count(int step_count) {
		this.step_count = step_count;
	}

	public EffectiveTimeFrame getEffective_time_frame() {
		return effective_time_frame;
	}

	public void setEffective_time_frame(EffectiveTimeFrame effective_time_frame) {
		this.effective_time_frame = effective_time_frame;
	}
}
