package com.googlefit.googlefit.model;

public class Token {

	private String token_uri;
	
	private String refresh_token;

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public String getToken_uri() {
		return token_uri;
	}

	public void setToken_uri(String token_uri) {
		this.token_uri = token_uri;
	}
}
