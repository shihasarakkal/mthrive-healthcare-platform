package com.googlefit.googlefit.model.elastic;

public class BodyFat {

	private EIndex index;
	
	private BodyFatPercentage body_fat_percentage;
	
	private EffectiveTimeFrame effective_time_frame;
	
	public BodyFat(EIndex index,BodyFatPercentage body_fat_percentage,EffectiveTimeFrame effective_time_frame){
		this.setIndex(index);
		this.body_fat_percentage=body_fat_percentage;
		this.effective_time_frame=effective_time_frame;
	}

	public BodyFatPercentage getBody_fat_percentage() {
		return body_fat_percentage;
	}

	public void setBody_fat_percentage(BodyFatPercentage body_fat_percentage) {
		this.body_fat_percentage = body_fat_percentage;
	}

	public EffectiveTimeFrame getEffective_time_frame() {
		return effective_time_frame;
	}

	public void setEffective_time_frame(EffectiveTimeFrame effective_time_frame) {
		this.effective_time_frame = effective_time_frame;
	}

	public EIndex getIndex() {
		return index;
	}

	public void setIndex(EIndex index) {
		this.index = index;
	}
}
