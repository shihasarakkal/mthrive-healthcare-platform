package com.googlefit.googlefit.model.elastic;

public class BodyFatPercentage {

	private String value;
	private String unit;
	
	public BodyFatPercentage(String value){
		this.value = value;
		this.unit = "%";
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
}
