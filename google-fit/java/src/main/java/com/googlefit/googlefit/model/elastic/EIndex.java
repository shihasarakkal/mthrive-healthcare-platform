package com.googlefit.googlefit.model.elastic;

public class EIndex {

	private String id;
	
	public EIndex(String id){
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
