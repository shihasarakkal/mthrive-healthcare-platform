package com.googlefit.googlefit.model.elastic;

public class EffectiveTimeFrame {

	private String date_time;

	public EffectiveTimeFrame(String date_time){
		this.setDate_time(date_time);
	}

	public String getDate_time() {
		return date_time;
	}

	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}
}
