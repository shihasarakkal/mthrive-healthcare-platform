# -*- coding: utf-8 -*-
"""
@author: vsuresh
"""

from math import ceil

class RhrFitnessLevelCalculator:
    rhr_chart = {
            'male': [[55, 61, 65, 69, 73, 81],
                     [54, 61, 65, 70, 74, 81],
                     [56, 62, 66, 70, 75, 82],
                     [57, 63, 67, 71, 76, 83],
                     [56, 61, 67, 71, 75, 81],
                     [55, 61, 65, 69, 73, 79]],

            'female': [[60, 65, 69, 73, 78, 84],
                       [59, 64, 68, 72, 76, 82],
                       [59, 64, 69, 73, 78, 84],
                       [60, 65, 69, 73, 77, 83],
                       [59, 64, 68, 73, 77, 83],
                       [59, 64, 68, 72, 76, 84]]
    }
    fitness_level_names = ['Athlete', 'Excellent', 'Good', 'Above Average',
                           'Average', 'Below Average', 'Poor']

    def age_group(self, x):
        return ceil((x - 25) / 10)

    def fitness_level(self, age, sex, rhr):
        level = sum(map(lambda x: rhr > x, self.rhr_chart[sex][self.age_group(age)]))
        level_centered = self.fitness_level_names.index('Average') - level
        return level_centered


from kafka import KafkaConsumer
from json import loads
import multiprocessing

class TrackerActivityAlertConsumer:
    def __init__(self, kf_serv_url_port, topic):
        self.consumer = KafkaConsumer(topic,
                                      enable_auto_commit=True,
                                      bootstrap_servers=[kf_serv_url_port],
                                      value_deserializer=lambda x: loads(x.decode('utf-8')))

    def handle_activity_async(self, activity_handler):
        with multiprocessing.Pool() as pool:
            for msg in self.consumer:
                pool.apply_async(activity_handler, args = (msg.value, ))

    def handle_activity(self, activity_handler):
        for msg in self.consumer:
            activity_handler(msg.value)


from elasticsearch import Elasticsearch

class UserInfoReader:
    def __init__(self, es_nodes, es_port, index, doc_type):
        self._es = Elasticsearch(es_nodes, port=es_port)
        self.index = index
        self.type = doc_type

    def get_user_info(self, user_id):
        user = self._es.get(index=self.index, doc_type=self.type, id=user_id)
        return user['_source']


class ElasticActivitySparkReader:
    def __init__(self, sc, es_nodes, es_port, es_resource):
        self.sc = sc
        self.es_read_conf = {
                "es.nodes": ','.join(es_nodes),
                "es.port": es_port,
                "es.resource": es_resource
        }

    def read_from_es(self, user_id):
        query = """
            {
              "query": {
                "bool": {
                  "must": [
                    { "match": { "header.user_id": "param_user_id" } },
                    { "range": { "body.effective_time_frame.date_time": { "gte": "now-2M" } } }
                  ]
                }
              }
            }
            """.replace('param_user_id', user_id)
        es_read_conf = dict(self.es_read_conf, **{'es.query': query})

        es_rdd = self.sc.newAPIHadoopRDD(
                    inputFormatClass="org.elasticsearch.hadoop.mr.EsInputFormat",
                    keyClass="org.apache.hadoop.io.NullWritable",
                    valueClass="org.elasticsearch.hadoop.mr.LinkedMapWritable",
                    conf=es_read_conf)
        return es_rdd


class SparkTrendElasticWriter:
    def __init__(self, sc, es_nodes, es_port, es_resource):
        self.sc = sc
        self.es_write_conf = {
                "es.nodes": ','.join(es_nodes),
                "es.port": es_port,
                "es.resource": es_resource,
                "es.mapping.id": "user_id_date_time",
                "es.mapping.exclude": "user_id_date_time"
        }

    def write_to_es(self, output_rdd):
        output_rdd.saveAsNewAPIHadoopFile(
                path='-',
                outputFormatClass="org.elasticsearch.hadoop.mr.EsOutputFormat",
                keyClass="org.apache.hadoop.io.NullWritable",
                valueClass="org.elasticsearch.hadoop.mr.LinkedMapWritable",
                conf=self.es_write_conf)

    def write_df_to_es(self, output_df):
        cols = output_df.columns
        cols.append('user_id_date_time')
        output_rdd = output_df \
            .selectExpr("*", "concat(user_id, '-', date_time) as user_id_date_time") \
            .rdd.map(lambda x: ('key', dict(zip(cols, x))))
        self.write_to_es(output_rdd)


from pyspark.sql import Row
from pyspark.sql.functions import datediff, to_date, current_date

class FitbitDataTransformer:
    calc = RhrFitnessLevelCalculator()

    def __init__(self, age, sex):
        self.fitness_level = lambda x: self.calc.fitness_level(age, sex, x)

    def in_transform(self, rdd):
        df = rdd.map(lambda x:
            Row(user_id = x[1]['header']['user_id'],
                level = self.fitness_level(x[1]['body']['heart_rate']['value']),
                date_time = x[1]['body']['effective_time_frame']['date_time'])) \
            .toDF() \
            .withColumn('int_time', datediff(to_date('date_time'), current_date()))
        return df

    def out_transform(self, df):
        return df.selectExpr('user_id', 'level', 'predicted_level', 'date_time')


from pyspark.ml.regression import LinearRegression
from pyspark.ml.feature import VectorAssembler

class TrendAnalyzer:
    features_col = "features"
    def __init__(self, response_col, predictor_cols, prediction_col='prediction'):
        self.response_col = response_col
        self.predictor_cols = predictor_cols
        self.prediction_col = prediction_col
        self.assembler = VectorAssembler(inputCols=predictor_cols,
                                         outputCol=self.features_col)

    def train(self, df):
        df = self.assembler.transform(df)
        lr = LinearRegression(labelCol=self.response_col,
                              featuresCol=self.features_col,
                              predictionCol=self.prediction_col,
                              solver='normal')
        lr_model = lr.fit(df)
        output = lr_model.transform(df)
        return Trend(lr_model, output)


class Trend:
    def __init__(self, lr_model, df):
        self.lr_model = lr_model
        self.df = df

    def healthy(self):
        beta_0 = self.lr_model.intercept
        return "Good" if beta_0 > 0 else "Poor"

    def trend(self, feature_idx=0, safe_slope=1/30):        # 1 level / 1 month
        certain = False
        beta_i = self.lr_model.coefficients[feature_idx]

        try:
            certain = self.lr_model.summary.pValues[feature_idx] <= 0.05   # CI interval 95%
        except:
            certain = self.lr_model.summary.r2 > 0.95            # 95% var explained

        if not certain:
            return "Uncertain"
        elif beta_i > safe_slope:
            return "Improving"
        elif beta_i < -safe_slope:
            return "Declining"
        else:
            return "No significant change"


class TrendSummaryBuilder:
    def __init__(self, es_nodes, es_port, index, doc_type):
        self._es = Elasticsearch(es_nodes, port=es_port)
        self.index = index
        self.doc_type = doc_type

    def __get_last_count(self, user_id):
        if self._es.exists(index=self.index, id=user_id):
            latest = self._es.get(index=self.index,
                                  id=user_id)
            if latest and 'count' in latest:
                return latest['count']
        return 0

    def build(self, trend):
        trend = {
            "user_id": trend.df.first().user_id,
            "date_time": trend.df.first().date_time,
            "health": trend.healthy(),
            "trend": trend.trend()
        }
        count = self.__get_last_count(trend['user_id'])
        if trend['health'] == 'Healthy' or trend['trend'] == 'Improving':
            trend['count'] = count + 1
        else:
            trend['count'] = 0
        return trend


class TrendSummaryWriter:
    def __init__(self, es_nodes, es_port, index, doc_type):
        self._es = Elasticsearch(es_nodes, port=es_port)
        self.index = index
        self.doc_type = doc_type

    def write(self, trend):
        self._es.index(index=self.index,
                       doc_type=self.doc_type,
                       id=trend['user_id'],
                       body=trend)


from kafka import KafkaProducer
from json import dumps

class HealthTrendNotify:
    def __init__(self, kf_serv_url_port, topic):
        self.topic = topic
        self._producer = KafkaProducer(bootstrap_servers=[kf_serv_url_port],
                                       value_serializer=lambda x: dumps(x).encode('utf-8'))

    def notify(self, trend, email):
        if trend['count'] > 0:
            trend.update(email = email)
            self._producer.send(self.topic, value=trend)


import traceback

class LogAsyncExceptions(object):
    def __init__(self, callable):
        self.__callable = callable
    def __call__(self, *args, **kwargs):
        try:
            result = self.__callable(*args, **kwargs)
        except Exception as e:
            multiprocessing.get_logger().error(traceback.format_exc())
            raise e
        return result



# bin/spark-submit --master local --packages org.elasticsearch:elasticsearch-hadoop:7.1.0,org.apache.spark:spark-sql_2.11:2.4.3 ~/SparkRegression.py
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession

conf = SparkConf().setAppName("Trend Analyzer")
sc = SparkContext(conf=conf)
spark = SparkSession(sc)

user_info_reader = UserInfoReader(['localhost'], '9200', 'user-profile', 'profile')
activity_reader = ElasticActivitySparkReader(sc, ['localhost'], '9200', 'daily-heartrate/fitbit')
analyzer = TrendAnalyzer('level', ['int_time'], 'predicted_level')
writer = SparkTrendElasticWriter(sc, ['localhost'], '9200', 'health-trend-series/fitbit')
summary_builder = TrendSummaryBuilder(['localhost'], '9200', 'health-trend-summary', 'fitbit')
summary_writer = TrendSummaryWriter(['localhost'], '9200', 'health-trend-summary', 'fitbit')
notifier = HealthTrendNotify('localhost:9092', 'health-trend-notify')
consumer = TrackerActivityAlertConsumer('localhost:9092', 'daily-heartrate-fitbit')

def handle_activity(activity):
    user_id = activity['user_id']
    print('Updating trend for user_id:', user_id)
    user_info = user_info_reader.get_user_info(user_id)
    activity_rdd = activity_reader.read_from_es(user_id)
    transformer = FitbitDataTransformer(user_info['age'], user_info['sex'])
    fitbit_activity_df = transformer.in_transform(activity_rdd)
    trend = analyzer.train(fitbit_activity_df)
    print(trend.lr_model.intercept, trend.lr_model.coefficients)
    fitbit_trend_df = transformer.out_transform(trend.df)
    writer.write_df_to_es(fitbit_trend_df)
    trend_summary = summary_builder.build(trend)
    print(trend_summary)
    summary_writer.write(trend_summary)
    notifier.notify(trend_summary, user_info['email'])
    print('Finished computing trend for user_id:', user_id)

#consumer.handle_activity_async(LogAsyncExceptions(handle_activity))
consumer.handle_activity(handle_activity)








